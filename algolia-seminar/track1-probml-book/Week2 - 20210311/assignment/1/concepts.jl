function mult(j, n)
    x = []
    i = 1
    while i * j < n
        push!(x, i * j)
        i += 1
    end
    return x
end

function powers(j, n)
    x = []
    i = 1
    while j^i < n
        push!(x, j^i)
        i += 1
    end
    return x
end

function ends(j, n)
    x = Array(1:n)
    x = filter(y -> y % 10 == j, x)
    return x
end

function squares(n)
    x = []
    i = 1
    while i^i < n
        push!(x, i^i)
        i += 1
    end
    return x
end

function getConcepts()
    Concepts = Dict(
        "all" => Array(1:100),
        "even" => Array(2:2:100),
        "odd" => Array(1:2:100),
        "powersOfTwo + {37}" => [2, 4, 8, 16, 32, 37, 64],
        "powersOfTwo - {32}" => [2, 4, 8, 16, 64],
        "squares" => squares(100)
    )

    for i in 3:10
        Concepts["Multiple of $i"] = mult(i, 100)
    end
    for i in 2:10
        Concepts["Powers of $i"] = powers(i, 100)
    end
    for i in 1:10
        Concepts["Ends in $i"] = ends(i, 100)
    end
    return Concepts
end

function getPriors()
    conceptKeys = keys(getConcepts())
    
    Priors = Dict(zip(conceptKeys, ones(length(conceptKeys))))
    Priors["powersOfTwo + {37}"] = 1.0 / 1000
    Priors["powersOfTwo - {32}"] = 1.0 / 1000
    Priors["odd"] = 100
    Priors["even"] = 100
    
    return Priors
end