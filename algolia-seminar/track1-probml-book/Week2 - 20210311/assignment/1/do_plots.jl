using PyPlot

function doPlots(concepts, priors, likelihoods, posteriors)
    @assert length(priors) == length(likelihoods) == length(posteriors)
    X = 1:length(priors)
    Range = range(0, stop=1, length=5)

    fig, ax = subplots(1, 3)
    ax[1].barh(X, priors)
    ax[1].set_yticklabels(concepts)
    ax[1].set_yticks(1:length(priors))
    ax[1].set_xticks(Range)
    ax[1].set_xticklabels(Range)
    ax[1].set_title("Prior")

    ax[2].barh(X, likelihoods)
    ax[2].set_yticklabels(repeat([""], length(concepts)))
    ax[2].set_yticks(1:length(priors))
    ax[2].set_title("Likelihood")

    ax[3].barh(X, posteriors)
    ax[3].set_yticklabels(repeat([""], length(concepts)))
    ax[3].set_yticks(1:length(priors))
    ax[3].set_xticks(Range)
    ax[3].set_xticklabels(Range)
    ax[3].set_title("Posterior")
    return fig
end