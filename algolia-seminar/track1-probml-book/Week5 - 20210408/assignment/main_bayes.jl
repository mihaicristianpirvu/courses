include("plots.jl")
# using Statistics

gauss(x; std, u) = 1 / (std * sqrt(2 * pi)) * exp( (-1 / 2) * ((x - u) / std) ^ 2 )
mean(x) = sum(x) / length(x)
std(x) = sqrt( mean( (x .- mean(x)).^2 ) )
discretize(f, start::Number, stop::Number; step::Number) = [f(x) for x in range(start, stop, step=step)]
abs(x::Array) = [abs(_x) for _x in x] 
abs(x::Number) = if(x > 0) x else -x end
cdf(X::Array; step::Number) = sum(X) * step

function doOneExperiment(N=100, P=0.5)
    return Array{Float64}(rand(N) .> P)
end

function headTails(e)
    heads = length(filter(x -> x == 1, e))
    tails = length(filter(x -> x == 0, e))
    return Dict("heads" => heads, "tails" => tails)
end

# P(H1)
function prior1()
    h1 = x -> gauss(x, std=1, u=-1.8)
end

# P(H2)
function prior2()
    h2 = x -> gauss(x, std=2, u=1.3)
end

# # P(Y|H1=h1)
function likelihood1(Y::Array, h)
    yMean, yStd = mean(Y), std(Y)
    g(x) = gauss(x, std=yStd, u=yMean)
    diff = sum(abs(discretize(x -> h - g(x), -10, 10, step=0.01))) .* 0.01
    return diff
end

function likelihood2(Y::Array, h::Float64)
    if h < 0 || h > 1
        return 0
    end

    mean(x) = sum(x) / length(x)
    A = mean(Y)
    B = abs(h - A)
    C = 1 - B
    D = 10
    # D = min(10, log(length(Y)))
    E = C ^ D 
    return E
end

function computeYsDisc(prior, likelihood; start, stop, step)
    cdf(discretize(x -> prior(x) * likelihood(x), start, stop, step=step), step=step)
end

e = doOneExperiment(100)
h1, h2 = prior1(), prior2()
l1 = x -> (h -> likelihood1(e, h))(h1(x))
l2 = x -> (h -> likelihood2(e, h))(h2(x))

YsDisc1 = computeYsDisc(h1, l1, start=-10, stop=10, step=0.1)
p1 = h -> h1(h) .* l1(h) ./ YsDisc1
YsDisc2 = computeYsDisc(h2, l2, start=-10, stop=10, step=0.1)
p2 = h -> h2(h) .* l2(h) ./ YsDisc2

Range = range(-5, 5, step=0.1)
doPlotsBayes(h1, h2, Range)
doPlotsBayes(l1, l2, Range)
doPlotsBayes(p1, p2, Range)
suptitle(headTails(e))
show()