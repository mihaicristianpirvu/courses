include("head_tails.jl")
using .HeadTails
using PyPlot

norm(X::Array) = X ./ sum(X)
mean(X::Array) = sum(X) / length(X)
grad(X::Array) = (X[2 : end] .- X[1 : end - 1]) ./ (length(X) - 1)
discretize(f, start::Number, stop::Number; step::Number) = [f(x) for x in range(start, stop, step=step)]
cdf(X::Array; step::Number) = sum(X) * step

function headTails(e)
    heads = length(filter(x -> x == 1, e))
    tails = length(filter(x -> x == 0, e))
    return Dict("heads" => heads, "tails" => tails)
end

function doPlots(e, prior, likelihood, posterior)
    start, stop = -5, 5
    stepDisc, stepCont = 0.1, 0.001
    XDisc = range(start, stop, step=stepDisc)
    XCont = range(start, stop, step=stepCont)
    priorDisc = discretize(prior, start, stop, step=stepDisc)
    priorCont = discretize(prior, start, stop, step=stepCont)
    likelihoodDisc = discretize(likelihood, start, stop, step=stepDisc)
    likelihoodCont = discretize(likelihood, start, stop, step=stepCont)
    posteriorDisc = discretize(posterior, start, stop, step=stepDisc)
    posteriorCont = discretize(posterior, start, stop, step=stepCont)

    println("CDF (Prior-Disc): ", cdf(priorDisc, step=stepDisc))
    println("CDF (Prior-Cont): ", cdf(priorCont, step=stepCont))
    println("CDF (Likelihood-Disc): ", cdf(likelihoodDisc, step=stepDisc))
    println("CDF (Likelihood-Cont): ", cdf(likelihoodCont, step=stepCont))
    println("CDF (Posterior-Disc): ", cdf(posteriorDisc, step=stepDisc))
    println("CDF (Posterior-Cont): ", cdf(posteriorCont, step=stepCont))

    fig, ax = subplots(1, 3)
    ax[1].barh(XDisc, priorDisc .* stepDisc, height=stepDisc * 0.95)
    ax[1].plot(priorCont .* stepDisc, XCont, color="black")
    ax[1].set_ylim(0, 1)
    ax[1].set_title("Prior")

    ax[2].barh(XDisc, likelihoodDisc .* stepDisc, height=stepDisc * 0.95)
    ax[2].plot(likelihoodCont .* stepDisc, XCont, color="black")
    ax[2].set_ylim(0, 1)
    ax[2].set_title("Likelihood")

    ax[3].barh(XDisc, posteriorDisc .* stepDisc, height=stepDisc * 0.95)
    ax[3].plot(posteriorCont .* stepDisc, XCont, color="black")
    ax[3].set_ylim(0, 1)
    ax[3].set_title("Posterior")

    suptitle("Dataset: $e")
end

function computeYsCont(prior, likelihood)
    # Ys = nothing
    # for i in 20:100:1000
    #     Hs = discretize(prior, i)
    #     Ys = [likelihood(Hs[i]) for i in 1:length(Hs)]
    #     uYs = mean(Ys)
    #     println(i, " ", uYs, " ", sum(grad(Ys)))
    # end

    # YsDisc = nothing
    # posterior = h -> (prior(h) .* likelihood(h))
    # for i in 20:100:1000
    #     YsDisc = discretize(posterior, i)
    #     Ys = sum(grad(YsDisc))
    #     println(i, " ", )    
    # end
    return 1
end

function computeYsDisc(prior, likelihood; start, stop, step)
    cdf(discretize(x -> prior(x) * likelihood(x), start, stop, step=step), step=step)
end

function main()
    e = HeadTails.doOneExperiment(10)
    println("Experiment: $e")
    
    # Prior funcion is defined as higher order function for each concept, yielding a guassian.
    prior = h -> HeadTails.prior()(h)

    # Higher order function where e is set in stone. Equivalent of partial(likelihood, e=e) in python
    likelihood = h -> HeadTails.likelihood(e, h)

    YsDisc = computeYsDisc(prior, likelihood, start=-10, stop=10, step=0.1)
    # TODO: HOW DO I INTEGRAL ???
    # YsCont = computeYsCont(prior, likelihood)
    posterior = h -> prior(h) .* likelihood(h) ./ YsDisc

    doPlots(headTails(e), prior, likelihood, posterior)
    show()

end

main()