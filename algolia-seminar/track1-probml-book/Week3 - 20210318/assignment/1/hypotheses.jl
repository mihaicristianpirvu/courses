# The "API" is that we have 3 functions: getConcepts() and prior() and likelihood(dataset, concept)
module Hypotheses

function mult(j::Int64, n::Int64)
    x = []
    i = 1
    while i * j < n
        push!(x, i * j)
        i += 1
    end
    return x
end

function powers(j::Int64, n::Int64)
    x = []
    i = 1
    while j^i < n
        push!(x, j^i)
        i += 1
    end
    return x
end

function ends(j::Int64, n::Int64)
    x = Array(1:n)
    x = filter(y -> y % 10 == j, x)
    return x
end

function squares(n::Int64)
    x = []
    i = 1
    while i^i < n
        push!(x, i^i)
        i += 1
    end
    return x
end

function conceptsSupports()
    Concepts = Dict(
        "all" => 1:100,
        "even" => 2:2:100,
        "odd" => 1:2:100,
        "powersOfTwo + {37}" => [2, 4, 8, 16, 32, 37, 64],
        "powersOfTwo - {32}" => [2, 4, 8, 16, 64],
        "squares" => squares(100)
    )

    for i in 3:10
        Concepts["Multiple of $i"] = mult(i, 100)
    end
    for i in 2:10
        Concepts["Powers of $i"] = powers(i, 100)
    end
    for i in 1:10
        Concepts["Ends in $i"] = ends(i, 100)
    end

    Keys = keys(Concepts)
    Values = map(x -> Array{Int64}(x), values(Concepts))
    return Dict(zip(Keys, Values))
end

function supports()
    return values(conceptsSupports())
end

function support(concept::String)
    return conceptsSupports()[concept]
end

function getConcepts()
    return Array{String}([x for x in keys(conceptsSupports())])
end

function priors()
    conceptKeys = getConcepts()

    Priors = Dict(zip(conceptKeys, ones(length(conceptKeys))))
    Priors["powersOfTwo + {37}"] = 1.0 / 1000
    Priors["powersOfTwo - {32}"] = 1.0 / 1000
    Priors["odd"] = 100
    Priors["even"] = 100

    normedPriors = values(Priors) ./ sum(values(Priors))
    return Dict(zip(conceptKeys, normedPriors))
end

function prior(concept::String)
    return priors()[concept]
end

# Size principle
function likelihood(experiment::Array{Int64}, support::Array{Int64})
    A = [x in support for x in experiment]
    A = prod(A)
    B = 1.0 / length(support)
    C = length(experiment)
    return A * B ^ C
end

function likelihood(experiment::Array{Int64}, concept::String)
    return likelihood(experiment, support(concept))
end

end