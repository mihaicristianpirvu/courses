module PrimeFreakness

function getConcepts()
    return Array(range(0, length=11, stop=1))
end

function getPriors()
    norm(x) = x ./ sum(x)
    concepts = getConcepts()
    x = ones(length(getConcepts()))
    x[3] = 5
    x = norm(x)
    return Dict(zip(concepts, x))
end

function prior(x::Float64)
    return getPriors()[x]
end

function allPrimesUnder100()
    return [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
end

function likelihood(experiment::Array{Int64}, concept::Float64)
    primes = allPrimesUnder100()
    numIn = sum([x in primes for x in experiment])
    A = numIn / length(experiment)
    B = abs(concept - A)
    C = (1 - B) ^ length(experiment)
    return C
end

end